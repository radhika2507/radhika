import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{MatButtonModule} from '@angular/material/button';
import{MatTableModule} from '@angular/material/table';
import{ HttpClientModule} from '@angular/common/http';
 import{MatFormFieldModule} from '@angular/material/form-field';
 import{FormsModule,ReactiveFormsModule} from '@angular/forms';
 import{MatInputModule} from '@angular/material/input';
import { MerchantModule } from './merchant/merchant.module';
import{Router,Params, RouterModule} from '@angular/router';
import {Ng2TelInputModule} from 'ng2-tel-input';


import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,    
    AppRoutingModule,
    BrowserAnimationsModule,MatButtonModule,MatTableModule,HttpClientModule,MatFormFieldModule,Ng2TelInputModule, FormsModule,ReactiveFormsModule,MatInputModule,MerchantModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
