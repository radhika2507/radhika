import { Component, OnInit ,Input,EventEmitter,Output, ViewChild} from '@angular/core';
import{Validators,FormControl,FormGroup,FormBuilder} from'@angular/forms'
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserdataService } from 'src/app/userdata.service';
import { AgmCoreModule } from '@agm/core';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
merchant:FormGroup
_id:any





  constructor( private user:UserdataService,  private route:Router, private path:ActivatedRoute) {
this.merchant=new FormGroup({
  name:new FormControl('',[Validators.required]),
  email :new FormControl('', [Validators.required, Validators.email]),
phone:new FormControl('',[Validators.required]),
dob:new FormControl('',[Validators.required]),
countrycode:new FormControl('',[Validators.required]),
address:new FormControl('',Validators.required)


});


this.path.params.subscribe((params:Params)=>this._id=params['_id']);
this.user.getbyid('merchant/id-here').subscribe((res:any)=>{
  this.merchant.patchValue(res.data);
  console.log(res);
});
    
    }

   adddata(){
    console.log(this.merchant.value);
     this.user.adddata('merchant',this.merchant.value).subscribe((res:any)=>{
      console.log(res)
      this.route.navigateByUrl('/list');
     })
    
   }
    updatedata(){
       this.user.update('merchant/id-here', this.merchant.value).subscribe((res:any)=>{
console.log(res);
 this.route.navigateByUrl('/list');
       })
    }
    
    

     
   
  ngOnInit(): void {
  }

}
