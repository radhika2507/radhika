import { Component, OnInit } from '@angular/core';

import{MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { UserdataService } from 'src/app/userdata.service';


export interface User {
  name:string
  email:any
  phone:number
  dob:number
  countryCode:any
  address:any
}
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  displayedColumns: string[] = ['name','email','phone','dob','countryCode','address','action'];
  dataSource = new MatTableDataSource<User>() 
  constructor(private data:UserdataService, private route:Router) { }


getlist(){
  this.data.getdata('merchant').subscribe((result:any)=>{
    console.log(result)
    this.dataSource=result.data.slice(0,5)
  })
}

  ngOnInit(): void {
    this.getlist();
  }
  

  delete(_id:string){
   
    this.data.deldata('merchant/id-here',+_id).subscribe((res:any)=>{
      console.log(res);
    
      });
    }
    editid(_id:string){
      console.log(_id,"element._id")
     this.route.navigate(['edit',+_id])
    }
     applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      console.log(filterValue);
      
    }
    
  }



