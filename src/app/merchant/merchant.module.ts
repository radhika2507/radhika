import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { matFormFieldAnimations, MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { Router, Params } from '@angular/router';
import { Ng2TelInputModule } from 'ng2-tel-input';
import{MatFormField} from '@angular/material/form-field'
import { HomeComponent } from './home/home.component';
import { AppComponent } from '../app.component';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { MerchantRoutingModule } from './merchant-routing.module';


@NgModule({
  declarations: [HomeComponent,AddComponent,ListComponent],
  imports: [
    MerchantRoutingModule,
    CommonModule, MatButtonModule, MatTableModule, MatFormFieldModule, HttpClientModule, ReactiveFormsModule, FormsModule, MatInputModule, Ng2TelInputModule
  ]
 
})
export class MerchantModule { }
