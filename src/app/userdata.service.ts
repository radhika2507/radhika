import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { url } from 'inspector';
import { isDelegatedFactoryMetadata } from '@angular/compiler/src/render3/r3_factory';
@Injectable({
  providedIn: 'root'
})
export class UserdataService {
baseurl=environment.base_url
  constructor(  private http:HttpClient) { }

getdata(url:any)
{
  return this.http.get(this.baseurl+url);
}

adddata(url:any,body:any){
  return this.http.post(this.baseurl+url,body);
}
deldata(url:any,_id:any)
{
  return this.http.delete(this.baseurl+url,_id)
} 
getbyid(url:string){
   return this.http.get(this.baseurl+url)
}
update(url:any,body:any){
   return this.http.put(this.baseurl+url,body)
}

}
